import MovieClasses.MovieFinder;
import MovieClasses.WebMovieFinder;

public class Main {

    public static void main(String[] args)
    {
        try {
            Container c = new Container();
            c.register(MovieFinder.class, WebMovieFinder.class);
            MovieFinder mf = c.newInstance(MovieFinder.class);
            System.out.println(mf.getMovieName());

            c.register("Valeur", 25);
            int v = (int)c.newInstance("Valeur");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
