import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class Container {
    HashMap<Class, Class> dependencies = new HashMap<>();
    HashMap<String, Object> values = new HashMap<>();
    HashMap<Class, Object> instances = new HashMap<>();


    <T, U> void register(Class<T> key, Class<U> dependency) {
        if (key.isInterface() && !dependency.isInterface()) {
            Class[] interfaces = dependency.getInterfaces();
            boolean implement = false;

            for (Class inter : interfaces) {
                if (inter == key) {
                    implement = true;
                }
            }
            if (implement) {
                dependencies.put(key, dependency);
            } else {
                throw new IllegalArgumentException("La classe " + dependency.getName() + " n'implemente pas l'interface " + key.getName());
            }
        } else if (key.isAssignableFrom(dependency) && dependency != key) {
            dependencies.put(key, dependency);
        } else {
            throw new IllegalArgumentException("La classe " + dependency.getName() + " n'herite pas de la classe " + key.getName());
        }
    }

    void register(String key, Object value) {
        if (value != null)
            values.put(key, value);

        else
            throw new IllegalArgumentException("La clef " + key + " est nulle");
    }

    <T> void register(Class<T> key){
        instances.put(key, null);
    }

    <T, U> T newInstance(Class<T> dependency) throws
            NoSuchMethodException,
            InstantiationException,
            IllegalAccessException,
            InvocationTargetException {

        /* On test si on a pas déjà instancié la dépendence */
        T newInstance = (T)instances.get(dependency);

        /* On test si une dépendance éxiste */
        if (newInstance == null){
            Class<?> instance = dependencies.get(dependency);

            if (instance == null){
                newInstance = dependency.getDeclaredConstructor().newInstance();
            }
            else{
                newInstance = (T)instance.getDeclaredConstructor().newInstance();
            }
        }

        instances.put(dependency, newInstance);

        return newInstance;
    }

    Object newInstance(String key) {
        return values.get(key);
    }
}
