package MovieClasses;

public class DbMovieFinder extends MovieFinder{
    public String name;

    public DbMovieFinder(){
        this.name = "DbMovieFinder";
    }

    public DbMovieFinder(String name){
        this.name = name;
    }

    @Override
    public String getMovieName() {
        return name;
    }
}
