package MovieClasses;

public class WebMovieFinder extends MovieFinder{
    public String name;

    public WebMovieFinder(){
        this.name = "WebMovieFinder";
    }

    public WebMovieFinder(String name){
        this.name = name;
    }

    @Override
    public String getMovieName() {
        return name;
    }
}
