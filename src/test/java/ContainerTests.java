import MovieClasses.DbMovieFinder;
import MovieClasses.MovieFinder;
import MovieClasses.WebMovieFinder;
import OtherClasses.A;
import OtherClasses.B;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ContainerTests {
    @DisplayName("Test1 : classe hérité")
    @Test
    void test1(){
        Container c = new Container();
        try{
            c.register(MovieFinder.class, WebMovieFinder.class);
            MovieFinder mf = c.newInstance(MovieFinder.class);
            assertEquals("WebMovieFinder", mf.getMovieName());
        }
        catch (Exception e){
            System.out.println(e.toString());
        }

    }

    @DisplayName("Test2 : classe avec interface")
    @Test
    void test2(){
        Container c = new Container();
        try{
            c.register(A.class, B.class);
            A a = c.newInstance(A.class);
            assertEquals("A", a.toString());
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    @DisplayName("Test3 : Classe sans dépendance")
    @Test
    void test3(){
        Container c = new Container();
        try{
            c.register(MovieFinder.class);
            MovieFinder mf = c.newInstance(MovieFinder.class);
            assertEquals("MovieFinder", mf.getMovieName());
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    @DisplayName("Test4 : Double dépendances")
    @Test
    void test4(){
        Container c = new Container();
        try{
            c.register(MovieFinder.class, WebMovieFinder.class);
            c.register(MovieFinder.class, DbMovieFinder.class);
            MovieFinder mf = c.newInstance(MovieFinder.class);
            assertEquals("DbMovieFinder", mf.getMovieName());
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    @DisplayName("Test5 : Classe non enregistrée dans le container")
    @Test
    void test5(){
        Container c = new Container();
        try{

            MovieFinder mf = c.newInstance(MovieFinder.class);

            assertEquals(mf.getMovieName(), "MovieFinder");
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    @DisplayName("Test6 : Instance déjà existante")
    @Test
    void test6(){
        Container c = new Container();
        try{
            c.register(MovieFinder.class);
            MovieFinder mf1 = c.newInstance(MovieFinder.class);
            MovieFinder mf2 = c.newInstance(MovieFinder.class);

            assertEquals(mf1, mf2);
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    @DisplayName("Test7 : Conteneur valeur + String")
    @Test
    void test7(){
        Container c = new Container();
        try{
            int test = 52;
            c.register("Valeur", test);
            int val = (int)c.newInstance("Valeur");
            assertEquals(test, val);
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    @DisplayName("Test8 : Test avec un héritage non implémenté")
    @Test
    void test8(){
        Container c = new Container();
        try{
            c.register(WebMovieFinder.class, MovieFinder.class);
            assertFalse(false);
        }
        catch (Exception e){
            System.out.println(e.toString());
            assertTrue(true);
        }
    }
}
